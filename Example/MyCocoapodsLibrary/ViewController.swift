//
//  ViewController.swift
//  MyCocoapodsLibrary
//
//  Created by ashok kumar chejarla on 11/13/2020.
//  Copyright (c) 2020 ashok kumar chejarla. All rights reserved.
//

import UIKit
import MyCocoapodsLibrary

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let log = Logger();
        log.printLog();
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

